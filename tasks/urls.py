from django.urls import path
from tasks.views import create_project, alltasks


urlpatterns = [
    path("create/", create_project, name="create_task"),
    path("mine/", alltasks, name="show_my_tasks"),
]
